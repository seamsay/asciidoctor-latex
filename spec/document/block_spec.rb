require 'asciidoctor'

RSpec.describe Asciidoctor::Converter::LaTeXConverter do
    context "with just preamble" do
        it "adds it as text to the start of the document" do
            document = <<~END
                = Some Title
                :reproducible:
                :sectnums:

                This is some preamble.

                == First Section
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    "This",
                    "is",
                    "some",
                    "preamble.",
                    "\n\n",
                    LaTeXParser::Command.new(:section, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with an abstract paragraph" do
        it "creates an abstract" do
            document = <<~END
                = Some Title
                :reproducible:
                :sectnums:

                [abstract]
                This is an abstract.

                == First Section
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Environment.new(:abstract, ["This", "is", "an", "abstract."]),
                    "\n\n",
                    LaTeXParser::Command.new(:section, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with an abstract paragraph and more preamble" do
        it "creates an abstract and adds the preamble after it" do
            document = <<~END
                = Some Title
                :reproducible:
                :sectnums:

                [abstract]
                This is an abstract.

                This is some preamble.

                == First Section
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Environment.new(:abstract, ["This", "is", "an", "abstract."]),
                    "\n\n",
                    "This",
                    "is",
                    "some",
                    "preamble.",
                    "\n\n",
                    LaTeXParser::Command.new(:section, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with the book doctype and a level 1 section" do
        it "creates a chapter" do
            document = <<~END
                = Some Title
                :doctype: book
                :reproducible:
                :sectnums:

                == First Chapter

                First paragraph.

                === First Section

                Second paragraph.
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["book"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Command.new(:chapter, [["First", "Chapter"]]),
                    LaTeXParser::Command.new(:label, [["_first_chapter"]]),
                    "\n\n",
                    "First",
                    "paragraph.",
                    "\n\n",
                    LaTeXParser::Command.new(:section, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                    "\n\n",
                    "Second",
                    "paragraph.",
                ]),
            ]

            converted = Asciidoctor.convert(document, header_footer: true, backend: 'latex')

            expect(LaTeXParser.parse(converted)).to eq(ast)
        end
    end

    context "with an appendix at the end of the document" do
        it "uses the \\appendix command" do
            document = <<~END
                = Some Title
                :reproducible:
                :sectnums:

                [appendix]
                == First Section
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Command.new(:appendix),
                    LaTeXParser::Command.new(:section, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                ]),
            ]

            converted = Asciidoctor.convert(document, header_footer: true, backend: 'latex')

            expect(LaTeXParser.parse(converted)).to eq(ast)
        end
    end

    context "with two appendices at the end of the document" do
        it "uses the \\appendix command exactly once" do
            document = <<~END
                = Some Title
                :reproducible:
                :sectnums:

                [appendix]
                == First Section

                [appendix]
                == Second Section
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Command.new(:appendix),
                    LaTeXParser::Command.new(:section, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                    "\n\n",
                    LaTeXParser::Command.new(:section, [["Second", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_second_section"]]),
                ]),
            ]

            converted = Asciidoctor.convert(document, header_footer: true, backend: 'latex')

            expect(LaTeXParser.parse(converted)).to eq(ast)
        end
    end

    context "with an appendix that has a subsection" do
        it "uses the \\appendix command" do
            document = <<~END
                = Some Title
                :reproducible:
                :sectnums:

                [appendix]
                == First Section

                === Subsection
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Command.new(:appendix),
                    LaTeXParser::Command.new(:section, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                    "\n\n",
                    LaTeXParser::Command.new(:subsection, [["Subsection"]]),
                    LaTeXParser::Command.new(:label, [["_subsection"]]),
                ]),
            ]

            converted = Asciidoctor.convert(document, header_footer: true, backend: 'latex')

            expect(LaTeXParser.parse(converted)).to eq(ast)
        end
    end

    context "with a non-appendix section after an appendix section" do
        it "raises an error" do
            document = <<~END
                = Some Title
                :reproducible:

                [appendix]
                == First Section

                == Second Section
            END

            expect { Asciidoctor.convert(document, header_footer: true, backend: 'latex') }.to raise_error RuntimeError
        end
    end

    context "with the sectnums attribute" do
        it "allows section numbers to be turned on and off" do
            document = <<~END
                = Some Title
                :reproducible:

                == First Section

                :sectnums:

                == Second Section

                :sectnums!:

                == Third Section
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Command.new(:addcontentsline, [["toc"], ["section"], ["First", "Section"]]),
                    LaTeXParser::Command.new(:section),
                    # TODO: Bad parsing.
                    "*", ["First", "Section"],
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                    "\n\n",
                    LaTeXParser::Command.new(:section, [["Second", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_second_section"]]),
                    "\n\n",
                    LaTeXParser::Command.new(:addcontentsline, [["toc"], ["section"], ["Third", "Section"]]),
                    LaTeXParser::Command.new(:section),
                    "*", ["Third", "Section"],
                    LaTeXParser::Command.new(:label, [["_third_section"]]),
                ]),
            ]

            converted = Asciidoctor.convert(document, header_footer: true, backend: 'latex')

            expect(LaTeXParser.parse(converted)).to eq(ast)
        end
    end

    context "with a discrete section" do
        it "generates an unnumbered section" do
            document = <<~END
                = Some Title
                :reproducible:
                :sectnums:

                [discrete]
                === Out-Of-Hierarchy Section

                == First Section
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Command.new(:subsection),
                    "*", ["Out-Of-Hierarchy", "Section"],
                    LaTeXParser::Command.new(:label, [["_out_of_hierarchy_section"]]),
                    "\n\n",
                    LaTeXParser::Command.new(:section, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                ]),
            ]

            converted = Asciidoctor.convert(document, header_footer: true, backend: 'latex')

            expect(LaTeXParser.parse(converted)).to eq(ast)
        end
    end

    context "with a floating chapter" do
        it "does not produce any invalid characters" do
            document = <<~END
                = Some Title
                :doctype: book
                :latex-document-class: report
                :reproducible:
                :sectnums:

                [discrete]
                == Out-Of-Hierarchy Section

                == First Section
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["report"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                    "\n\n",
                    LaTeXParser::Command.new(:chapter),
                    "*", ["Out-Of-Hierarchy", "Section"],
                    LaTeXParser::Command.new(:label, [["_out_of_hierarchy_section"]]),
                    "\n\n",
                    LaTeXParser::Command.new(:chapter, [["First", "Section"]]),
                    LaTeXParser::Command.new(:label, [["_first_section"]]),
                ]),
            ]

            converted = Asciidoctor.convert(document, header_footer: true, backend: 'latex')

            expect(LaTeXParser.parse(converted)).to eq(ast)
        end
    end
end
