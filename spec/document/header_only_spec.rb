require 'asciidoctor'

RSpec.describe Asciidoctor::Converter::LaTeXConverter do
    context "with just a title" do
        it "creates a document with just a title" do
            document = <<~END
                = Some Title
                :reproducible:
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with a title containing special LaTeX characters" do
        it "escapes the characters correctly" do
            document = <<~END
                = {1, 2, 3} Is A Set Of (Random Backslash \\\\) Numbers, 50% Of Which Are Odd, Worth Approximately $0 \\^_^
                :reproducible:
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [[
                    LaTeXParser::Command.new(:"{"),
                    "1,", "2,", "3",
                    LaTeXParser::Command.new(:"}"),
                    "Is", "A", "Set", "Of", "(Random", "Backslash",
                    [LaTeXParser::Command.new(:textbackslash)],
                    ")", "Numbers,", "50",
                    LaTeXParser::Command.new(:"%"),
                    "Of", "Which", "Are", "Odd,", "Worth", "Approximately",
                    LaTeXParser::Command.new(:"$"),
                    "0",
                    [LaTeXParser::Command.new(:textasciicircum)],
                    # TODO: This is not parsed correctly, but the output is correct.
                    LaTeXParser::Command.new(:"_", [[LaTeXParser::Command.new(:textasciicircum)]]),
                ]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with a title and subtitle" do
        it "creates a document with a correctly formatted subtitle" do
            document = <<~END
                = Some Title: And Subtitle
                :reproducible:
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title:", "And", "Subtitle"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with an author" do
        it "creates a document with an author" do
            document = <<~END
                = Some Title
                S. Author
                :reproducible:
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Command.new(:author, [["S.", "Author"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with an author and email" do
        it "correctly formats the email" do
            document = <<~END
                = Some Title
                S. Author <some.email@server.org>
                :reproducible:
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Command.new(:author, [["S.", "Author", '\\\\', 'some.email@server.org']]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with multiple authors" do
        it "uses the \\and command" do
            document = <<~END
                = Some Title
                A. One; A. Two
                :reproducible:
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Command.new(:author, [["A.", "One", LaTeXParser::Command.new(:and, [], []), "A.", "Two"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with multiple authors and emails" do
        it "uses the \\and command and formats the email correctly" do
            document = <<~END
                = Some Title
                A. One <first@email.com>; B. Two <second@email.com>
                :reproducible:
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Command.new(:author, [["A.", "One", "\\\\", "first@email.com", LaTeXParser::Command.new(:and, [], []), "B.", "Two", "\\\\", "second@email.com"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with a date" do
        it "adds the \\date command" do
            document = <<~END
                = Some Title
                A. One
                2020-12-15
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Command.new(:author, [["A.", "One"]]),
                LaTeXParser::Command.new(:date, [["2020-12-15"]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with no date and reproducible not set" do
        it "uses \\today" do
            document = <<~END
                = Some Title
                A. One
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["article"]], ["a4paper,11pt"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Command.new(:author, [["A.", "One"]]),
                LaTeXParser::Command.new(:date, [[LaTeXParser::Command.new(:today)]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end

    context "with a specified class and options" do
        it "sets the \\documentclass macro correctly" do
            document = <<~END
                = Some Title
                A. One
                :doctype: book
                :latex-document-class: report
                :latex-document-options: 12pt,a4paper
            END

            ast = [
                LaTeXParser::Command.new(:documentclass, [["report"]], ["12pt,a4paper"]),
                LaTeXParser::Command.new(:title, [["Some", "Title"]]),
                LaTeXParser::Command.new(:author, [["A.", "One"]]),
                LaTeXParser::Command.new(:date, [[LaTeXParser::Command.new(:today)]]),
                LaTeXParser::Environment.new(:document, [
                    LaTeXParser::Command.new(:maketitle),
                ]),
            ]

            expect(LaTeXParser.parse(Asciidoctor.convert(document, header_footer: true, backend: 'latex'))).to eq(ast)
        end
    end
end
