let
  pkgs = import <nixpkgs> { };
  environment = pkgs.bundlerEnv {
    name = "asciidoctor-latex";

    inherit (pkgs) ruby;
    gemdir = ./.;
  };
in environment.env
