begin
    require_relative 'lib/asciidoctor-latex/version.rb'
rescue LoadError
    require 'asciidoctor-latex/version.rb'
end

Gem::Specification.new do |spec|
    spec.name = 'asciidoctor-latex'
    spec.version = Asciidoctor::LaTeX::VERSION
    spec.authors = ["Sean Marshallsay"]
    spec.summary = "A simple LaTeX backend for Asciidoctor."

    spec.files = Dir['**/**'].grep_v(/.gem$/)

    spec.require_paths = ["lib"]

    spec.add_runtime_dependency("asciidoctor", "~> 2.0")
    spec.add_runtime_dependency("asciimath", "~> 2.0")
    spec.add_runtime_dependency("htmlentities", "~> 4.3")

    spec.add_development_dependency("minitest", "~> 5.14")
    spec.add_development_dependency("rspec", "~> 3.0")
end