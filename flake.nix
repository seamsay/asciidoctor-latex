
{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};

      environment = pkgs.bundlerEnv {
        name = "asciidoctor-latex";
        gemdir = ./.;
      };
    in {
     # devShells.default = pkgs.mkShell {
     #    shellHook = ''
     #      export ASCIIDOCTOR=${environment}/bin/asciidoctor
     #    '';
     #  };
     devShells.default = environment.env;

      formatter = pkgs.alejandra;
    });
}
