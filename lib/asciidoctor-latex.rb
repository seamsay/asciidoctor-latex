require 'asciidoctor'
require 'asciidoctor/extensions'
require 'asciimath'

def highlighter_package(asciidoctor, latex)
    # TODO: Use warnings instead of raising.
    raise '`latex-source-highlighter` must be either `minted` or `listings`.' unless latex.nil? || ['minted', 'listings'].include?(latex)

    if asciidoctor == 'pygments' || latex == 'minted'
        :minted
    elsif latex == 'listings'
        :listings
    else
        :verbatim
    end
end

# Test whether a string represents an integer without raising.
def integer?(string)
    !Integer(string, exception: false).nil?
end

def section_title(doctype, level)
    name = if doctype == 'book' && level == 1
        'chapter'
    else
        'section'
    end

    subs = if doctype == 'book'
        [0, level - 2].max
    else
        level - 1
    end

    [name, subs]
end

# TODO: Find a way to ensure that calling texify is easy to get right and hard to get wrong.
#       In particular it should be impossible to forget to call it when needed, and impossible to call it when it shouldn't be called.
#       I think the best way to do this is to wrap every string coming from Asciidoctor in an AsciidoctorString class and every string in the converter with either TeXString or RawSting depending on what escapes need to be made.
#       I have tried doing this before in the StringWrappers branch, but I'm not good enough at either Ruby or Asciidoctor to do it successfully.
# TODO: More of this needs testing.
# Stolen shamelessly from the manify converter in the main asciidoctor codebase.
# See the following lists of special LaTeX characters:
# * https://web.archive.org/web/20210504111859/https://tex.stackexchange.com/questions/34580/escape-character-in-latex/34586
# * https://web.archive.org/web/20210504111722/https://www.desigeek.com/blog/amit/2012/02/28/less-than-symbol-in-latex/
# * https://web.archive.org/web/20210504112126/https://math-linux.com/latex-26/faq/latex-faq/article/latex-horizontal-space-qquad-hspace-thinspace-enspace
# * https://web.archive.org/web/20210504114121/https://en.wikipedia.org/wiki/Zero-width_space
# This module is designed in such a way that you can't escape characters in the wrong order, be aware of that when refactoring or fixing bugs.
module TeXify
    class PlainString
        def initialize(string)
            # Ensure that string is in fact something that is supposed to act like a string.
            @value = string.to_str
        end

        def escape_characters_produced_by_entity_conversion
            # Entity conversion can introduce both backslashes and tildes, so they must be escaped before entities.
            EscapedEntityConversions.new(
                @value
                    .gsub(/[\{\}]/, '\\\\\&')
                    .gsub(/\\[^\{\}]/, '{\textbackslash}')
                    .gsub('~', '{\textasciitilde}')
            )
        end
    end

    class TeXString
        def initialize(string)
            @value = string.to_str
        end

        def initialise_no_escape_needed
            EscapedEntityConversions.new(@value)
        end
    end

    class EscapedEntityConversions
        def initialize(string)
            @value = string
        end

        def convert_entity_pairs
            # These regexes contain a zero-width space, so must be converted before the zero-width space.
            em_dash_regex = /&#8212;(?:&#8203;)?/
            ellipsis_regex = /&#8230;(?:&#8203;)?/

            ConvertedEntityPairs.new(
                @value
                    .gsub(em_dash_regex, '{\textemdash}')
                    .gsub(ellipsis_regex, '{\textellipsis}')
            )
        end
    end

    class ConvertedEntityPairs
        def initialize(string)
            @value = string
        end

        def convert_entities
            ConvertedEntities.new(
                @value
                    .gsub('&amp;', '\\\&')  # NOTE: I have no idea why three backslashes are needed (as opposed to the expected two).
                    .gsub('&lt;', '{\textless}')
                    .gsub('&gt;', '{\textgreater}')
                    .gsub('&#160;', '~')  # Non-breaking space.
                    .gsub('&#169;', '{\textcopyright}')
                    .gsub('&#174;', '{\textregistered}')
                    .gsub('&#8482;', '{\texttrademark}')
                    .gsub('&#8201;', '{\thinspace}')
                    .gsub('&#8203;', '\hspace{0pt}') # Zero-width space.
                    .gsub('&#8211;', '{\textendash}')
                    .gsub('&#8216;', '{\textquoteleft}')
                    .gsub('&#8217;', '{\textquoteright}')
                    .gsub('&#8220;', '{\textquotedblleft}')
                    .gsub('&#8221;', '{\textquotedblright}')
                    .gsub('&#8592;', '{\textleftarrow}')
                    .gsub('&#8594;', '{\textrightarrow}')
                    .gsub('&#8656;', '\(\Leftarrow\)')  # Left double arrow. TODO: Can double arrows be done without using math mode?
                    .gsub('&#8658;', '\(\Rightarrow\)')  # Right double arrow.
            )
        end
    end

    class ConvertedEntities
        def initialize(string)
            @value = string
        end

        def finalise_no_escape_needed
            @value
        end

        def escape_entity_characters
            # Entities are marked by `&#`, so these characters must be escaped after entities are converted.
            EscapedEntityCharacters.new(
                @value
                    .gsub(/[\&#]/, '\\\\\&')
            )
        end
    end

    class EscapedEntityCharacters
        def initialize(string)
            @value = string
        end

        def escape_remaining
            @value
                .gsub('^', '{\textasciicircum}')
                .gsub(/[%\$_]/, '\\\\\&')
        end
    end
end

def texify(string)
    string and TeXify::PlainString.new(string)
        .escape_characters_produced_by_entity_conversion
        .convert_entity_pairs
        .convert_entities
        .escape_entity_characters
        .escape_remaining
end

# To be used in cases where there might be TeX characters already in the text.
def texify_entities(string)
    string and TeXify::TeXString.new(string)
        .initialise_no_escape_needed
        .convert_entity_pairs
        .convert_entities
        .finalise_no_escape_needed
end

# TODO: Docinfo stuff is not tested.
module Asciidoctor
    class Converter::LaTeXConverter < Converter::Base
        register_for 'latex', 'tex'

        def initialize(backend, opts = {})
            @backend = backend
            init_backend_traits(basebackend: 'tex', filetype: 'tex', outfilesuffix: '.tex')

            @appendix_started = false
        end

        alias convert_pass content_only
        alias convert_preamble content_only
        # TODO: Probably needs some special casing, like in the HTML5 backend.
        alias convert_embedded content_only

        def convert_dlist(list)
            result = []

            result << '\begin{description}'
            list.items.each do |terms, description|
                # TODO: Support multiple terms.
                if terms.length != 1
                    raise "Description lists can currently only contain one term."
                end
                term = terms[0]
                result << "\\item[#{term.text}] #{description.text}"
                result << description.content if description.blocks?
            end
            result << '\end{description}'

            result.join LF
        end

        def convert_document(document)
            # TODO: Handle subtitles too.
            title = document.doctitle
            authors = document.authors
            # TODO: Handle `revnumber` and `revremark`.
            date = if document.revdate.nil?
                if document.attr? "reproducible"
                    nil
                else
                    '\today'
                end
            else
                document.revdate
            end

            result = []

            document_class = if document.attr? 'latex-document-class'
                document.attr 'latex-document-class'
            elsif !(['article', 'book'].include?(document.attr 'doctype'))
                'article'
            else
                document.attr 'doctype'
            end

            if (document_class == 'report' || document_class == 'book') && document.attr('doctype') != 'book'
                raise "Document classes that have chapters must use Asciidoctor's book doctype."
            end

            document_options = document.attr('latex-document-options', 'a4paper,11pt')
            result << "\\documentclass[#{document_options}]{#{document_class}}"

            unless document.docinfo().empty?
                result << document.docinfo
            end

            result << "\\title{#{texify title}}"

            unless authors.nil? || authors.empty?
                string = authors.map do |author|
                    string = author.name
                    unless author.email.nil?
                        string += ' \\\\ '
                        string += author.email
                    end
                    string
                end.join(' \and ')
                result << "\\author{#{string}}"
            end

            unless date.nil?
                result << "\\date{#{date}}"
            end

            result.concat([
                '\begin{document}',
                '\maketitle',
            ])

            unless document.docinfo(:header).empty?
                result << document.docinfo(:header)
            end

            result << document.content

            unless document.docinfo(:footer).empty?
                result << document.docinfo(:footer)
            end

            result << '\end{document}'

            result.join(LF)
        end

        def convert_floating_title(title)
            result = []

            name, subs = section_title(title.document.attr('doctype'), title.level)
            result << '\\' + ('sub' * subs) + "#{name}*{#{texify title.title}}"
            result << "\\label{#{title.id}}"
            result << title.content

            result.join LF
        end

        def convert_image(image)
            file = image.attr 'target'
            file.slice! File.extname(file)

            # TODO: Fall back to scaledwidth.
            pdfwidth = image.attr('pdfwidth', nil)
            htmlwidth = image.attr('width', nil)

            width = if pdfwidth.nil?
                if htmlwidth.nil?
                    nil
                else
                    if htmlwidth.end_with? '%'
                        "#{htmlwidth.to_f / 100}\\textwidth"
                    elsif htmlwidth.end_with?('px') || integer?(htmlwidth)
                        raise 'LaTeX does not support using pixels as units.'
                    else
                        raise "PDF width not specified and width (`#{htmlwidth}`) is invalid."
                    end
                end
            else
                if pdfwidth.end_with? 'px'
                    raise "LaTeX does not support using pixels as units."
                elsif pdfwidth.end_with? '%'
                    "#{pdfwidth.to_f / 100}\\textwidth"
                elsif pdfwidth.end_with? 'pt'
                    # Compare https://asciidoctor.org/docs/user-manual/#pdfwidth to https://en.wikibooks.org/wiki/LaTeX/Lengths#Units.
                    "#{pdfwidth[0..-3]}bp"
                elsif pdfwidth.end_with? 'vw'
                    "#{pdfwidth.to_f / 100}\\paperwidth"
                elsif pdfwidth.end_with?('pc') || pdfwidth.end_with?('cm') || pdfwidth.end_with?('mm') || pdfwidth.end_with?('in') || integer?(pdfwidth)
                    # Asciidoctor assumes points when their are no units, while `\includegraphics` assumes big points, but as above they are equivalent so no need for conversions.
                    pdfwidth
                else
                    raise "PDF width (`#{pdfwidth}`) is invalid."
                end
            end

            includegraphics = ['\includegraphics']

            unless width.nil?
                includegraphics << "[width=#{width}]"
            end

            includegraphics << "{#{file}}"
            includegraphics = includegraphics.join ''

            result = []

            result << '\begin{figure}[h]'
            result << '\centering'
            result << includegraphics

            if image.title?
                # NOTE: `captioned_title` must not be used so that latex can manage referencing.
                # TODO: This used to call `texify`, but that doesn't play nicely with macros. This needs testing.
                result << "\\caption{#{texify_entities image.title}}"

                unless image.id.nil?
                    result << "\\label{#{image.id}}"
                end
            elsif !image.id.nil?
                raise "LaTeX can't reference a figure if it doesn't have a caption."
            end

            result << '\end{figure}'

            result.join LF
        end

        def convert_inline_anchor(node)
            case node.type
            when :xref
                refid = node.attributes['refid']
                ref_node = node.document.references[:refs][refid]
                ref_string = "\\ref{#{refid}}"

                if ref_node.nil?
                    return ref_string
                end

                xrefstyle = node.document.attr('latex-xrefstyle', node.document.attr('xrefstyle'))

                prefix = case ref_node.node_name
                when 'section'
                    name, _ = section_title(ref_node.document.attr('doctype'), ref_node.level)
                    if ref_node.attr('style') == 'appendix'
                        'Appendix'
                    else
                        name.capitalize
                    end
                when 'image', 'listing'
                    'Figure'
                else
                    raise "References to `#{ref_node.node_name}` nodes not yet implemented."
                end

                if xrefstyle == 'short' || xrefstyle.nil?
                    "#{prefix}~\\ref{#{refid}}"
                else
                    raise "`xrefstyle` '#{xrefstyle}' is not currently supported."
                end
            when :link
                if node.target == node.text
                    "\\url{#{node.target}}"
                else
                    "\\href{#{node.target}}{#{node.text}}"
                end
            else
                raise "Unknown inline anchor type `#{node.type}`."
            end
        end

        def convert_inline_quoted(node)
            if node.type == :asciimath
                "\\(#{AsciiMath::parse(node.text).to_latex}\\)"
            elsif node.type == :latexmath
                "\\(#{node.text}\\)"
            else
                text = texify node.text
                case node.type
                when :latexmath
                    raise "`:latexmath` handled elsewhere."
                when :asciimath
                    raise "`:asciimath` handled elsewhere."
                when :monospaced
                    "\\texttt{#{text}}"
                when :emphasis
                    "\\emph{#{text}}"
                when :strong
                    "\\textbf{#{text}}"
                when :mark
                    "\\hl{#{text}}"
                when :single
                    # NOTE: We don't use `text' syntax here because when we do Asciidoctor seems to turn multiple occurences of double quoted words into verbatim text.
                    "{\\textquoteleft}#{text}{\\textquoteright}"
                when :double
                    # NOTE: We don't use ``text'' syntax here because when we do Asciidoctor seems to turn multiple occurences of double quoted words into verbatim text.
                    "{\\textquotedblleft}#{text}{\\textquotedblright}"
                when :subscript
                    "\\textsubscript{#{text}}"
                when :superscript
                    "\\textsuperscript{#{text}}"
                else
                    raise "Unknown quoted type `#{node.type}`."
                end
            end
        end

        def convert_listing(block)
            # TODO: Why doesn't `block.document.syntax_highlighter` work? It always returns `nil`...
            package = highlighter_package(
                (block.document.syntax_highlighter && block.document.syntax_highlighter.name),
                block.document.attr('latex-source-highlighter'),
            )

            result = []

            # TODO: Can listings and minted be implemented as custom syntax highlighters?
            if block.style != 'source' || package == :verbatim
                inner = []

                inner << '\begin{verbatim}'
                inner << texify(block.content)
                inner << '\end{verbatim}'

                if block.title?
                    result << '\begin{figure}[h]'

                    result.concat inner

                    result << "\\caption{#{texify block.title}}"

                    unless block.id.nil?
                        result << "\\label{#{block.id}}"
                    end

                    result << '\end{figure}'
                elsif !block.id.nil?
                    raise "LaTeX can't reference a figure if it doesn't have a caption."
                else
                    result.concat inner
                end
            elsif package == :listings
                beginning = []

                beginning << '\begin{lstlisting}'
                if block.attr?('language') || block.title? || block.attr?('linenums')
                    beginning << '['

                    options = []

                    if block.attr? 'language'
                        options << "language=#{block.attr 'language'}"
                    end

                    if block.title?
                        options << "caption={#{texify block.title}}"

                        unless block.id.nil?
                            options << "label=#{block.id}"
                        end
                    elsif !block.id.nil?
                        raise "LaTeX can't reference a figure if it doesn't have a caption."
                    end

                    if block.attr? 'linenums'
                        options << 'numbers=left'
                    end

                    beginning << options.join(',')
                    beginning << ']'
                end

                result << beginning.join('')
                result << texify(block.content)
                result << '\end{lstlisting}'
            elsif package == :minted
                if !block.attr? 'language'
                    raise "Minted requires a language on source blocks."
                end

                beginning = []

                beginning << '\begin{minted}'

                if block.attr? 'linenums'
                    beginning << '[linenos]'
                end

                beginning << "{#{block.attr 'language'}}"

                inner = []

                inner << beginning.join('')
                inner << texify(block.content)
                inner << '\end{minted}'

                if block.title?
                    result << '\begin{listing}[h]'

                    result.concat inner

                    result << "\\caption{#{texify block.title}}"

                    unless block.id.nil?
                        result << "\\label{#{block.id}}"
                    end

                    result << '\end{listing}'
                elsif !block.id.nil?
                    raise "LaTeX can't reference a figure if it doesn't have a caption."
                else
                    result.concat inner
                end
            else
                raise "Could not figure out the LaTeX package to use."
            end

            result.join LF
        end

        def convert_olist(list)
            result = []

            result << '\begin{enumerate}'
            # TODO: This only works for the outermost enumeration environment.
            #       Two options, either track the nesting depth (could that be done with a class variable?) or use a package like enumerate or enumitem (though I don't know for certain that they offer the functionality).
            #       Probably the former is best.
            result << "\\setcounter{enumi}{#{list.attr 'start'}}" if list.attr? 'start'
            list.items.each do |item|
                result << "\\item #{item.text}"
                if item.blocks?
                    raise "Nesting not currently supported in enumerations."
                end
            end
            result << '\end{enumerate}'
        end

        def convert_open(block)
            if block.style == 'abstract'
                result = []

                result << '\begin{abstract}'
                result << texify_entities(block.content)
                result << '\end{abstract}'

                # TODO: Check whether an empty line is needed after an abstract.
                result << "\n"

                result.join LF
            else
                texify_entities block.content
            end
        end

        def convert_page_break(_)
            # NOTE: https://tex.stackexchange.com/a/497776
            '\clearpage'
        end

        def convert_paragraph(paragraph)
            texify_entities(paragraph.content) + LF
        end

        def convert_section(title)
            if @appendix_started && title.level == 1 && title.attr('style') != 'appendix'
                raise "LaTeX can only have appendices at the end of the document."
            end

            if title.attr('style') == 'appendix' && title.level != 1
                raise "LaTeX can only start the appendix with a level one section."
            end

            # TODO: Test `sectnumlevels`.
            max_num = title.document.attr('sectnumlevels', 3).to_i
            numbered = title.numbered && title.level <= max_num
            asterisk = if numbered
                ''
            else
                '*'
            end

            result = []

            if !@appendix_started && title.attr('style') == 'appendix'
                @appendix_started = true
                result << '\appendix'
            end

            name, subs = section_title(title.document.attr('doctype'), title.level)
            level = ('sub' * subs) + name

            texified_title = texify title.title

            if !numbered
                result << "\\addcontentsline{toc}{#{level}}{#{texified_title}}"
            end

            result << "\\#{level}#{asterisk}{#{texified_title}}"
            result << "\\label{#{title.id}}"
            result << title.content

            result.join LF
        end

        def convert_stem(stem)
            result = []

            result << '\['
            if stem.style == 'latexmath'
                result << stem.content
            else
                result << AsciiMath::parse(stem.content).to_latex
            end
            result << '\]'

            result.join LF
        end

        def convert_thematic_break(_)
            '\par\noindent\rule{\textwidth}{0.5pt}'
        end

        def convert_ulist(list)
            result = []

            result << '\begin{itemize}'
            list.items.each do |item|
                result << "\\item #{item.text}"
                result << item.content if item.blocks?
            end
            result << '\end{itemize}'

            result.join LF
        end
    end
end

# == Problem
#
# Asciidoctor doesn't give the converter any information about where blank lines appear in the document.
#
# Consider the following AsciiDoc excerpt:
# ----
# The equation
# [stem]
# ++++
# y = mx + c
# ++++
# describes a straight line.
# ----
# Asciidoctor sees this as three seperate nodes:
#
# . A paragraph: `The equation`.
# . A stem block: `y = mx + c`.
# . A paragraph: `describes a straight line.`.
#
# This means that if converted naively, the result will be
# ----
# The equation
#
# \[
# y = mx + c
# \]
# describes a straight line.
# ----
# which LaTeX would see as two nodes, one of which has two subnodes:
#
# . A paragraph: `The equation`.
# . A paragraph:
# .. A display equation: `y = mx + c`.
# .. Some text: `describes a straight line.`.
#
# What a LaTeX author probably wants, however, is
# ----
# The equation
# \[
# y = mx + c
# \]
# describes a straight line.
# ----
# which is a _single_ paragraph containing a display equation.
#
# == Solution
#
# The solution is actually quite simple, though I'm not yet convinced that there won't be any edge cases to it.
#
# . Preprocess the document.
# .. Find some string that does not appear in the text.
# .. Replace any blank line with that string surrounded by blank lines.
# . Postprocess the document.
# .. Remove all blank lines.
# .. Replace any instance of the string with a blank line.
#
# Expanding slightly on the previous example, let's consider this excerpt:
# ----
# The equation
# [stem]
# ++++
# y = mx + c
# ++++
# describes a straight line.
#
# The equation
# [latexmath]
# ++++
# sum_i a_i x_i + b = 0
# ++++
# is a more general form of linear equation.
# ----
# After preprocessing this document will look like this:
# ----
# The equation
# [stem]
# ++++
# y = mx + c
# ++++
# describes a straight line.
#
# ASCIIDOCTOR_LATEX_BLANK_LINE_0
#
# The equation
# [latexmath]
# ++++
# sum_i a_i x_i + b = 0
# ++++
# is a more general form of linear equation.
# ----
# Then it will be converted into:
# ----
# The equation
#
# \[
# y = mx + c
# \]
# describes a straight line.
#
# ASCIIDOCTOR_LATEX_BLANK_LINE_0
#
# The equation
#
# \[
# sum_i a_i x_i + b = 0
# \]
# is a more general form of linear equation.
# ----
# Postprocessing will then fix it to be
# ----
# The equation
# \[
# y = mx + c
# \]
# describes a straight line.
#
# The equation
# \[
# sum_i a_i x_i + b = 0
# \]
# is a more general form of linear equation.
# ----
# which is what we desire.

ASCIIDOCTOR_LATEX_BLANK_LINE_TOKEN_PREFIX = "ASCIIDOCTORLATEXBLANKLINE"
ASCIIDOCTOR_LATEX_BLANK_LINE_TOKENS = {}

class TeXBlankLinePreprocessor < Asciidoctor::Extensions::Preprocessor
    def process(document, reader)
        return reader if reader.eof? || !document.basebackend?('tex')

        if !ASCIIDOCTOR_LATEX_BLANK_LINE_TOKENS[document].nil?
            raise "Document appears to have already been preprocessed without being postprocessed... something wierd has happened..."
        end

        largest = -1
        reader.lines.each do |line|
            match = /#{ASCIIDOCTOR_LATEX_BLANK_LINE_TOKEN_PREFIX}(\d+)/.match(line)
            if !match.nil?
                current = match.captures.first.to_i
                if current > largest
                    largest = current
                end
            end
        end

        ASCIIDOCTOR_LATEX_BLANK_LINE_TOKENS[document] = ASCIIDOCTOR_LATEX_BLANK_LINE_TOKEN_PREFIX + (largest + 1).to_s

        lines = []
        reader.read_lines.each do |line|
            if line == ''
                lines << ""
                lines << ASCIIDOCTOR_LATEX_BLANK_LINE_TOKENS[document]
                lines << ""
            else
                lines << line
            end
        end
        reader.unshift_lines lines

        reader
    end
end

class TeXBlankLinePostprocessor < Asciidoctor::Extensions::Postprocessor
    def process(document, output)
        if document.basebackend? 'tex'
            output = output.gsub(/^\n/, '')
            output = output.gsub(/#{ASCIIDOCTOR_LATEX_BLANK_LINE_TOKENS[document]}/, '')

            ASCIIDOCTOR_LATEX_BLANK_LINE_TOKENS.delete document

            output
        else
            output
        end
    end
end

Asciidoctor::Extensions.register do |registry|
    if registry.document.basebackend? 'tex'
        registry.preprocessor TeXBlankLinePreprocessor
        registry.postprocessor TeXBlankLinePostprocessor
    end
end
